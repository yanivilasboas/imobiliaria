import React from 'react';
import { BrowserRouter, Switch, Route } from "react-router-dom";

import Home from "../pages/HomeADM/index";

import { Cliente } from '../pages/Clientes/index';
import Clientes from "../pages/Listagem/clientes";
import { Imovel } from "../pages/Imoveis/index";
import Imoveis from "../pages/Listagem/imoveis";
import ImoveisAlugados from "../pages/Listagem/imoveisAlugados";
import { Proprietario } from '../pages/Proprietarios';
import Proprietarios from "../pages/Listagem/proprietarios";
import { Locacao } from '../pages/Locacao';
import Locacoes from "../pages/Listagem/contratos"

export default function Routes() {

  
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/homeADM" component={Home} />
        <Route exact path="/cliente" component={Cliente} />
        <Route exact path="/clientes" component={Clientes} />
        <Route exact path="/imovel" component={Imovel} />
        <Route exact path="/imoveis" component={Imoveis} />
        <Route exact path="/imoveis_alugados" component={ImoveisAlugados} />
        <Route exact path="/proprietario" component={Proprietario} />
        <Route exact path="/proprietarios" component={Proprietarios} />
        <Route exact path="/locacao" component={Locacao} />
        <Route exact path="/locacoes" component={Locacoes} />

      </Switch>
    </BrowserRouter>
  );
}
