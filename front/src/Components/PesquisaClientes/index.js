import React, { useState } from 'react';
import clienteService from "../../services/clienteService";
import styles from '../PesquisaClientes/styles.module.scss'

import { InputGroup, FormControl, Button, Form, Col } from 'react-bootstrap';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useEffect } from 'react';


function PesquisaClientes(props) {

    const [search, setSearch] = useState('');

    const [clientes, setClientes] = useState([]);

    useEffect(() => {
        refreshClientes();
        return () => {
        }
    }, [])

    async function refreshClientes() {
        clienteService.retrieveAllClientes()
            .then(
                response => {
                    setClientes(response.data)
                }

            )
    }

    function handleOnSubmit(event) {
        event.preventDefault();
        const results = clientes.filter(cliente => cliente.nome.toLowerCase().indexOf(search) !== -1);
        props.setClientes(results);
    }

    function handleSearchChange(event) {
        setSearch(event.target.value.toLowerCase());
    }
    return (
        <div>
            <Form onSubmit={handleOnSubmit} className={styles.pesquisa}>
                <Form.Row>
                    <Col>
                        <InputGroup>
                            <FormControl
                                placeholder="Pesquise por nome"
                                aria-label="Pesquise por nome"
                                onChange={handleSearchChange}
                            />
                            <InputGroup.Append>
                                <Button type="submit">
                                    <FontAwesomeIcon icon={faSearch} />
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Col>
                </Form.Row>
            </Form>
        </div>
    );
}

export default PesquisaClientes;