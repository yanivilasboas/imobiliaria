import styles from '../ListItemImoveis/styles.module.scss'
import { Row, Col } from 'react-bootstrap';

function ListItem(props) {
    return (
        <div className={styles.listagem}>
            <Row>
  
                <Col md={12}>
               
                    <h1 class="text-center"><b>Tipo: </b> {props.tipo}</h1>
               
                    <h1 class="text-center"><b>Valor: </b> R${props.valor}</h1>
                    
                    <h1 class="text-center"><b>Bairro: </b> {props.bairro}</h1>
                </Col>
            </Row>

        </div>
    );
}

export default ListItem;