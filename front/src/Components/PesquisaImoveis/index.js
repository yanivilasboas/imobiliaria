import React, { useState } from 'react';
import imovelService from "../../services/imovelService";
import styles from '../PesquisaClientes/styles.module.scss'

import { InputGroup, FormControl, Button, Form, Col } from 'react-bootstrap';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useEffect } from 'react';


function PesquisaImoveis(props) {

    const [search, setSearch] = useState('');

    const [imoveis, setImoveis] = useState([]);

    useEffect(() => {
        refreshImoveis();
        return () => {
        }
    }, [])

    async function refreshImoveis() {
        imovelService.retrieveAllImoveis()
            .then(
                response => {
                    setImoveis(response.data)
                }

            )
    }

    function handleOnSubmit(event) {
        event.preventDefault();
        const results = imoveis.filter(imovel=> imovel.tipo.toLowerCase().indexOf(search) !== -1 ||
                                                imovel.bairro.toLowerCase().indexOf(search) !== -1);
        props.setImoveis(results);
    }

    function handleSearchChange(event) {
        setSearch(event.target.value.toLowerCase());
    }
    return (
        <div>
            <Form onSubmit={handleOnSubmit} className={styles.pesquisa}>
                <Form.Row>
                    <Col>
                        <InputGroup>
                            <FormControl
                                placeholder="Pesquise por tipo ou bairro"
                                aria-label="Pesquise por tipo ou bairro"
                                onChange={handleSearchChange}
                            />
                            <InputGroup.Append>
                                <Button type="submit">
                                    <FontAwesomeIcon icon={faSearch} />
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Col>
                </Form.Row>
            </Form>
        </div>
    );
}

export default PesquisaImoveis;