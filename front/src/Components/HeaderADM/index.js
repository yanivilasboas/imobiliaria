import styles from '../HeaderADM/styles.module.scss'
import { Dropdown, SplitButton } from 'react-bootstrap';
import '../../App.css';


export function Header() {



  return (
    <div className={styles.headerContainer}>
      
      <header >
        <h3 onClick={(event) => { event.preventDefault(); window.open("/"); }}> Imobiliária Vilas Boas </h3>
      </header>

      <div>
        {['end'].map((direction) => (
          <SplitButton
            key={direction}
            drop={direction}
            title={`Clientes`}
          >
            <div className={styles.item}>
              <Dropdown.Item className={styles.itemMenu} eventKey="1" href="/cliente">Cadastrar</Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item className={styles.itemMenu} eventKey="2" href="/clientes">Ver Listagem</Dropdown.Item>
            </div>
          </SplitButton>
        ))}
      </div>

      <div>
        {['end'].map((direction) => (
          <SplitButton
            key={direction}
            drop={direction}
            title={`Imóveis`}
          >
            <div className={styles.item}>
              <Dropdown.Item className={styles.itemMenu} eventKey="1" href="/imovel">Cadastrar</Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item className={styles.itemMenu} eventKey="2" href="/imoveis_alugados">Imóveis Alugados</Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item className={styles.itemMenu} eventKey="3" href="/imoveis">Imóveis Cadastrados</Dropdown.Item>
            </div>
          </SplitButton>
        ))}
      </div>
      <div>
        {['end'].map((direction) => (
          <SplitButton
            key={direction}
            drop={direction}
            title={`Locação`}
          >
            <div className={styles.item}>
              <Dropdown.Item className={styles.itemMenu} eventKey="1" href="/locacao">Registrar Locação</Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item className={styles.itemMenu} eventKey="2" href="/locacoes">Ver Contratos</Dropdown.Item>
            </div>
          </SplitButton>
        ))}
      </div>

      <div>
        {['end'].map((direction) => (
          <SplitButton
            key={direction}
            drop={direction}
            title={`Proprietários`}
          >
            <div className={styles.item}>
              <Dropdown.Item className={styles.itemMenu} eventKey="1" href="/proprietario">Cadastrar</Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item className={styles.itemMenu} eventKey="2" href="/proprietarios">Ver Listagem</Dropdown.Item>

            </div>
          </SplitButton>
        ))}
      </div>

      
    </div>

  );
}