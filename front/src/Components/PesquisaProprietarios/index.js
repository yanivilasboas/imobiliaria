import React, { useState } from 'react';
import proprietarioService from "../../services/proprietarioService";
import styles from '../PesquisaProprietarios/styles.module.scss'

import { InputGroup, FormControl, Button, Form, Col } from 'react-bootstrap';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useEffect } from 'react';


function PesquisaProprietarios(props) {

    const [search, setSearch] = useState('');

    const [proprietarios, setProprietarios] = useState([]);

    useEffect(() => {
        refreshProprietarios();
        return () => {
        }
    }, [])

    async function refreshProprietarios() {
        proprietarioService.retrieveAllProprietarios()
            .then(
                response => {
                    setProprietarios(response.data);
                }

            )
    }

    function handleOnSubmit(event) {
        event.preventDefault();
        const results = proprietarios.filter(proprietario => proprietario.nome.toLowerCase().indexOf(search) !== -1);
        props.setProprietarios(results);
    }

    function handleSearchChange(event) {
        setSearch(event.target.value.toLowerCase());
    }
    return (
        <div>
            <Form onSubmit={handleOnSubmit} className={styles.pesquisa}>
                <Form.Row>
                    <Col>
                        <InputGroup>
                            <FormControl
                                placeholder="Pesquise por nome"
                                aria-label="Pesquise por nome"
                                onChange={handleSearchChange}
                            />
                            <InputGroup.Append>
                                <Button type="submit">
                                    <FontAwesomeIcon icon={faSearch} />
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Col>
                </Form.Row>
            </Form>
        </div>
    );
}

export default PesquisaProprietarios;