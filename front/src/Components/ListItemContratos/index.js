import styles from '../ListItemContratos/styles.module.scss'
import { Row, Col } from 'react-bootstrap';

function ListItem(props) {
    return (
        <div className={styles.listagem}>
            <Row>
  
                <Col md={12}>
                    <h1 class="text-center"><b> {props.imovel} - {props.cliente}</b></h1>
                </Col>
            </Row>

        </div>
    );
}

export default ListItem;