import React, { useState } from 'react';
import projetoService from "../../services/projetoService";
import styles from '../PesquisaProjetos/styles.module.scss'

import { InputGroup, FormControl, Button, Form, Col } from 'react-bootstrap';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useEffect } from 'react';


function PesquisaProjetos(props) {

    const [search, setSearch] = useState('');

    const [projetos, setProjetos] = useState([]);

    useEffect(() => {
        refreshProjetos();
        return () => {
        }
    }, [])

    async function refreshProjetos() {
        projetoService.retrieveAllProjetos()
            .then(
                response => {
                    setProjetos(response)
                }

            )
    }

    function handleOnSubmit(event) {
        event.preventDefault();
        const results = projetos.filter(projeto => projeto.titulo.toLowerCase().indexOf(search) !== -1
            || projeto.palavrasChave.toLowerCase().indexOf(search) !== -1
            || projeto.areaConhecimento.area.toLowerCase().indexOf(search) !== -1
            || projeto.edital.descricao.toLowerCase().indexOf(search) !== -1
            || projeto.edital.numero.toString().indexOf(search) !== -1
            || projeto.edital.ano.toString().indexOf(search) !== -1);
        props.setProjetos(results);
    }

    function handleSearchChange(event) {
        setSearch(event.target.value.toLowerCase());
    }
    return (
        <div>
            <Form onSubmit={handleOnSubmit} className={styles.pesquisa}>
                <Form.Row>
                    <Col>
                        <InputGroup>
                            <FormControl
                                placeholder="Pesquise por título, edital, palavras-chave ou área do conhecimento"
                                aria-label="Pesquise por título, edital, palavras-chave ou área do conhecimento"
                                onChange={handleSearchChange}
                            />
                            <InputGroup.Append>
                                <Button type="submit">
                                    <FontAwesomeIcon icon={faSearch} />
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Col>
                </Form.Row>
            </Form>
        </div>
    );
}

export default PesquisaProjetos;