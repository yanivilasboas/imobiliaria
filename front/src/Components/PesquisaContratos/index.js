import React, { useState } from 'react';
import locacaoService from "../../services/locacaoService";
import styles from '../PesquisaContratos/styles.module.scss'

import { InputGroup, FormControl, Button, Form, Col } from 'react-bootstrap';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useEffect } from 'react';


function PesquisaContratos(props) {

    const [search, setSearch] = useState('');

    const [contratos, setContratos] = useState([]);

    useEffect(() => {
        refreshContratos();
        return () => {
        }
    }, [])

    async function refreshContratos() {
        locacaoService.retrieveAllLocacoes()
            .then(
                response => {
                    setContratos(response.data)
                }

            )
    }

    function handleOnSubmit(event) {
        event.preventDefault();
        const results = contratos.filter(contrato=> contrato.imovel.tipo.toLowerCase().indexOf(search) !== -1 ||
                                                    contrato.imovel.bairro.toLowerCase().indexOf(search) !== -1||
                                                    contrato.cliente.nome.toLowerCase().indexOf(search) !== -1);
        props.setContratos(results);
    }

    function handleSearchChange(event) {
        setSearch(event.target.value.toLowerCase());
    }
    return (
        <div>
            <Form onSubmit={handleOnSubmit} className={styles.pesquisa}>
                <Form.Row>
                    <Col>
                        <InputGroup>
                            <FormControl
                                placeholder="Pesquise por cliente, tipo de imóvel ou bairro"
                                aria-label="Pesquise por cliente, tipo de imóvel ou bairro"
                                onChange={handleSearchChange}
                            />
                            <InputGroup.Append>
                                <Button type="submit">
                                    <FontAwesomeIcon icon={faSearch} />
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Col>
                </Form.Row>
            </Form>
        </div>
    );
}

export default PesquisaContratos;