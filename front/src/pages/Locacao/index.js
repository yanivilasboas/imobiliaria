import "antd/dist/antd.css"
import { useState, useEffect } from 'react'
import styles from '../Locacao/styles.module.scss'
import React from 'react';

import { Row, Col } from 'react-bootstrap';

import { Form, message, Input, Select } from 'antd';

import { Header } from '../../Components/HeaderADM';

import LocacaoService from "../../services/locacaoService";
import imovelService from "../../services/imovelService";
import clienteService from "../../services/clienteService";
import locacaoService from "../../services/locacaoService";

const { Option } = Select;

export function Locacao(props) {

  const [imovel, setImovel] = useState([]);
  const [imoveis, setImoveis] = useState([]);

  const [cliente, setCliente] = useState([]);
  const [clientes, setClientes] = useState([]);

  const [locacoes, setLocacoes] = useState([]);

  const [form] = Form.useForm();

  useEffect(() => {
    refresh();
    return () => {
    }
  }, [])

  async function refresh() {
    clienteService.retrieveAllClientes()
      .then(
        response => {
          setClientes(response.data);
        }
      )

    imovelService.retrieveAllImoveis()
      .then(
        response => {
          setImoveis(response.data);
        }
      )

    locacaoService.retrieveAllLocacoes()
      .then(
        response => {
          setLocacoes(response.data);
        }
      )
  }

  const onFinish = (values) => {


    form.resetFields();

    values.cliente = {
      idCliente: cliente
    };

    values.imovel = {
      idImovel: imovel
    };

    values.contrato = "Ativo";

    LocacaoService.saveLocacoes(values).then(() => {
      message.success("Locação registrada com sucesso!")
    })
      .catch((error) => {
        message.error("Locação não foi concluída!")
      })



  }

  function handleChangeImovel(value) {
    setImovel(value);
  }

  function handleChangeCliente(value) {
    setCliente(value);
  }



  return (
    <div className={styles.wrapper}>

      <Header />

      <main className={styles.App}>
        <h1>Registar Locação</h1>

        <Form form={form} className={styles.stepsForm} onFinish={onFinish}>

          <div className={styles.fieldsContainer}>

            <Row className={styles.fields}>

              <Col md={6} className={styles.field}>
                <Form.Item
                  label="Imóvel:"
                  name="imovel.idImovel"
                  rules={[
                    {
                      required: true,
                      message: 'Informe o imóvel',
                    },
                  ]}>
                  <Select onChange={handleChangeImovel} >
                    <Option value="" disabled selected>Selecione o imóvel</Option>
                    {imoveis.map((item) => {
                      return (
                        <Option key={item.idImovel} value={item.idImovel} >
                          {item.tipo} - {item.bairro} / {item.idImovel}
                        </Option>
                      )
                    })}
                  </Select>
                </Form.Item>
              </Col>

              <Col md={6} className={styles.field}>
                <Form.Item
                  label="Cliente:"
                  name="cliente.idCliente"
                  rules={[
                    {
                      required: true,
                      message: 'Informe o cliente',
                    },
                  ]}>
                  <Select onChange={handleChangeCliente} >
                    <Option value="" disabled selected>Selecione o cliente</Option>
                    {clientes.map((item) => {
                      return (
                        <Option key={item.idCliente} value={item.idCliente} >
                          {item.nome}
                        </Option>
                      )
                    })}
                  </Select>
                </Form.Item>
              </Col>


              <Col md={6} className={styles.field}>
                <Form.Item
                  label="Data de Ínicio do contrato:"
                  name="data_inicio"
                  rules={[
                    {
                      required: true,
                      message: 'Insira a data de início',
                    },
                  ]}>
                  <Input type="date" />
                </Form.Item>
              </Col>

              <Col md={6} className={styles.field}>
                <Form.Item
                  label="Data de Finalização do Contrato:"
                  name="data_fim"
                >
                  <Input type="date" />
                </Form.Item>
              </Col>

              <Col md={12} >
                <button className={styles.buttonSubmit} type="submit" >
                  Salvar
                </button>
              </Col>
            </Row>
          </div>
        </Form>
      </main>
    </div>
  );

}
