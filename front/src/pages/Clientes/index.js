import "antd/dist/antd.css"
import styles from '../Clientes/styles.module.scss'
import React from 'react';

import { Row, Col } from 'react-bootstrap';

import { Form, message, Input } from 'antd';
import InputMask from "react-input-mask";
import { Header } from '../../Components/HeaderADM';

import ClienteService from "../../services/clienteService";

const InputTel = (props) => (
  <InputMask mask="(99)99999-9999"
    value={props.value}
    onChange={props.onChange}
    placeholder="Insira o número de telefone" />
);


export function Cliente(props) {

  const [form] = Form.useForm();

  const onFinish = (values) => {
    
    form.resetFields();

    ClienteService.saveClientes(values).then(() => {
      message.success("Cliente cadastrado com sucesso!")
    })
      .catch((error) => {
        message.error("Cadastro não foi concluído!")
      });
  }

  return (
  <div className={styles.wrapper}>

  <Header />

  <main className={styles.App}>
    <h1>Cadastrar Clientes</h1>

    <Form form={form} className={styles.stepsForm} onFinish={onFinish}>

      <div className={styles.fieldsContainer}>

        <Row className={styles.fields}>

          <Col md={12} className={styles.field}>
            <Form.Item
              label="Nome:"
              name="nome"
              rules={[
                {
                  required: true,
                  message: 'Insira o nome completo',
                },
              ]}>
              <Input placeholder="Insira o nome completo" />
            </Form.Item>
          </Col>

          <Col md={6} className={styles.field}>
            <Form.Item
              label="Email:"
              name="email"
              rules={[
                {
                  required: true,
                  message: 'Insira o endereço de email',
                },
              ]}>
              <Input type="email" placeholder="Insira o endereço de email" />
            </Form.Item>
          </Col>

          <Col md={6} className={styles.field}>
            <Form.Item
              label="Telefone:"
              name="telefone"
              rules={[
                {
                  required: true,
                  message: 'Insira o número de telefone',
                },
              ]}>
              <InputTel />
            </Form.Item>
          </Col>
      
          <Col md={12} >
            <button className={styles.buttonSubmit} type="submit" >
              Salvar
            </button>
          </Col>
        </Row>
      </div>
    </Form>
  </main>
</div>
 );

}
