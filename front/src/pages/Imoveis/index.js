import "antd/dist/antd.css"
import styles from '../Imoveis/styles.module.scss'
import React from 'react';

import { useState, useEffect } from 'react'

import { Row, Col } from 'react-bootstrap';

import { Form, message, Input, Select} from 'antd';
import InputMask from "react-input-mask";
import { Header } from '../../Components/HeaderADM';

import ImovelService from "../../services/imovelService";
import ProprietarioService from "../../services/proprietarioService";

const InputCEP = (props) => (
  <InputMask mask="99999-999"
    value={props.value}
    onChange={props.onChange}
    placeholder="Insira o CEP" />
);

const { Option } = Select;

export function Imovel(props) {

  const [proprietario, setProprietario] = useState([]);
  const [proprietarios, setProprietarios] = useState([]);

  const [form] = Form.useForm();
  
  useEffect(() => {
    refresh();
    return () => {
    }
  }, [])
  
  function handleChange(value) {
    setProprietario(value);
  }

  async function refresh() {
    ProprietarioService.retrieveAllProprietarios()
      .then(
        response => {
          setProprietarios(response.data);
        }
      )
  }

  const onFinish = (values) => {
    
    form.resetFields();

    values.proprietario = {
      idProprietario: proprietario
    };


    ImovelService.saveImoveis(values).then(() => {
      message.success("Imóvel cadastrado com sucesso!")
    })
      .catch((error) => {
        message.error("Cadastro não foi concluído!")
      });
  }

  return (
  <div className={styles.wrapper}>

  <Header />

  <main className={styles.App}>
    <h1>Cadastrar Imóveis</h1>

    <Form form={form} className={styles.stepsForm} onFinish={onFinish}>

      <div className={styles.fieldsContainer}>

        <Row className={styles.fields}>

        <Col md={4} className={styles.field}>
                <Form.Item
                  label="Tipo de Imóvel:"
                  name="tipo"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o tipo de imóvel',
                    },
                  ]}>
                  <select >
                    <option value="" disabled selected>Selecione o tipo</option>
                    <option value="Apartamento">Apartamento</option>
                    <option value="Casa">Casa</option>
                    <option value="Kitnet">Kitnet</option>
                  </select>
                </Form.Item>
              </Col>


              
              <Col md={4} className={styles.field}>
                <Form.Item
                  label="Proprietário:"
                  name="proprietario.idProprietario"
                  rules={[
                    {
                      required: true,
                      message: 'Informe o nome',
                    },
                  ]}>
                  <Select onChange={handleChange} >
                    <Option value="" disabled selected>Selecione o nome</Option>
                    {proprietarios.map((item) => {
                      return (
                        <Option key={item.idProprietario} value={item.idProprietario} >
                          {item.nome}
                        </Option>
                      )
                    })}
                  </Select>
                </Form.Item>
              </Col>

              <Col md={4} className={styles.field}>
                <Form.Item
                  label="Valor do aluguel:"
                  name="valor"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o valor',
                    },
                  ]}>

                <Input placeholder="Insira o valor" type="number"/>
                  
                </Form.Item>
              </Col>

              <Col md={10} className={styles.field}>
                <Form.Item
                  label="Logradouro:"
                  name="logradouro"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o endereço!',
                    },

                  ]}>
                  <Input placeholder="Insira o endereço" />
                </Form.Item>
              </Col>
              <Col md={2} className={styles.field}>
                <Form.Item
                  label="Nº:"
                  name="numero"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o número!',
                    },

                  ]}>
                  <Input placeholder="Insira o número" />
                </Form.Item>
              </Col>
              <Col md={3} className={styles.field}>
                <Form.Item
                  label="Bairro:"
                  name="bairro"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o bairro!',
                    },

                  ]}>
                  <Input placeholder="Insira o bairro" />
                </Form.Item>
              </Col>
              <Col md={3} className={styles.field}>
                <Form.Item
                  label="Cidade:"
                  name="cidade"
                  rules={[
                    {
                      required: true,
                      message: 'Insira a cidade!',
                    },

                  ]}>
                  <Input placeholder="Insira a cidade" />
                </Form.Item>
              </Col>
              <Col md={2} className={styles.field}>
                <Form.Item
                  label="CEP:"
                  name="cep"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o CEP!',
                    },

                  ]}>
                  <InputCEP/>
                </Form.Item>
              </Col>
              <Col md={3} className={styles.field}>
                <Form.Item
                  label="Estado:"
                  name="estado"
                  rules={[
                    {
                      required: true,
                      message: 'Insira o Estado!',
                    },

                  ]}>
                  <Input placeholder="Insira o Estado" />
                </Form.Item>
              </Col>
              <Col md={1} className={styles.field}>
                <Form.Item
                  label="UF:"
                  name="uf"
                  rules={[
                    {
                      required: true,
                      message: 'Insira a UF!',
                    },

                  ]}>
                  <Input maxLength="2"/>
                </Form.Item>
              </Col>

         
      
          <Col md={12} >
            <button className={styles.buttonSubmit} type="submit" >
              Salvar
            </button>
          </Col>
        </Row>
      </div>
    </Form>
  </main>
</div>
 );

}
