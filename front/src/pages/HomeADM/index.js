import styles from '../HomeADM/styles.module.scss'
import { Header } from '../../Components/HeaderADM';
import { Row, Col } from 'react-bootstrap';

import { useState, useEffect } from "react";
import DashboardService from "../../services/dashboardService";

export default function Home() {
    const [dashboard, setDashboard] = useState([]);

    async function refresh() {
      DashboardService.dashboard().then((response) => {
        setDashboard(response.data);
      });
    }
  
    useEffect(() => {
      refresh();
      return () => { };
    }, []);


    return (
        <div className={styles.wrapper}>
            <Header />
            <main className={styles.homeContainer}>

                <h1 className={styles.titulo}>Imobiliária Vilas Boas</h1>

                <Row className={styles.menu}>

                    <Col md={3} className={styles.itemMenu}>

                        <div class="card  bg-success mb-3" onClick={(event) => { event.preventDefault(); window.open("/clientes"); }}>

                        <div class="card-header"><h4 >Quantidade de Clientes</h4></div>
                <div class="card-body">
                  <h5 className={styles.impar}> {dashboard.qtdClientes}</h5>
                </div>
                        </div>

                    </Col>

                    <Col md={3} className={styles.itemMenu} >

                        <div class="card  bg-success mb-3" onClick={(event) => { event.preventDefault(); window.open("/imoveis"); }}>

                           
                        <div class="card-header"><h4 >Quantidade de Imóveis</h4></div>
                <div class="card-body">
          
                  <h5 className={styles.impar}>{dashboard.qtdImoveis}</h5>
                </div>
                        </div>

                    </Col>

                    <Col md={3} className={styles.itemMenu} >
                        <div class="card  bg-success mb-3" onClick={(event) => { event.preventDefault(); window.open("/locacoes"); }}>

                           
                        <div class="card-header"><h4 >Quantidade de Locações</h4></div>
                <div class="card-body">
                 
                  <h5 className={styles.impar}> {dashboard.qtdLocacoes}</h5>
                </div>
                        </div>

                    </Col>

                    <Col md={3} className={styles.itemMenu} >
                        <div class="card  bg-success mb-3" onClick={(event) => { event.preventDefault(); window.open("/proprietarios"); }}>

                            
                        <div class="card-header"><h4 >Quantidade de Proprietários</h4></div>
                <div class="card-body">
                  <h5 className={styles.impar}>{dashboard.qtdProprietarios}</h5>
                </div>

                        </div>
                    </Col>

                </Row>
            </main>
        </div>
    );
}

