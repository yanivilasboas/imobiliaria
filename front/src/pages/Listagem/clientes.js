import styles from '../Listagem/styles.module.scss'
import ListItem from '../../Components/ListItemClientes';
import clienteService from "../../services/clienteService";
import PesquisaClientes from '../../Components/PesquisaClientes';
import InputMask from "react-input-mask";

import { Form, Input, message, Button } from 'antd';
import { Modal } from "react-bootstrap";
import { Header } from '../../Components/HeaderADM';
import { useEffect, useState } from 'react';
import { Row, Col } from 'react-bootstrap';


const InputTel = (props) => (
    <InputMask mask="(99)99999-9999"
        value={props.value}
        onChange={props.onChange}
        placeholder="Insira o número de telefone" />
);

export default function Clientes() {

    const [ativo, setAtivo] = useState(false);
    const [ativoAtualizar, setAtivoAtualizar] = useState(false);
    const [ativoConfirmar, setAtivoConfirmar] = useState(false);
    const handleClose = () => setAtivo(false);
    const handleCloseAtualizar = () => setAtivoAtualizar(false);
    const handleCloseConfirmar = () => setAtivoConfirmar(false);
    const [clientes, setClientes] = useState([]);
    const [cliente, setCliente] = useState([]);

    useEffect(() => {
        refreshClientes();
        return () => {
        }
    }, [])

    const [form] = Form.useForm();

    const onFinish = (values) => {

        handleCloseAtualizar();
        form.resetFields();

        values.idCliente = cliente.idCliente;

        if (values.nome == null) {
            values.nome = cliente.nome;
        }
        if (values.email == null) {
            values.email = cliente.email;
        }
        if (values.telefone == null) {
            values.telefone = cliente.telefone;
        }

        clienteService.updateClientes(values).then(() => {
            refreshClientes();
            message.success("Cadastro Atualizado com sucesso!")
        })
            .catch((error) => {
                message.error("Cadastro não atualizado!")
            });
    }

    const excluirCadastro = (values) => {

        handleCloseConfirmar();
        clienteService.deleteCliente(values.idCliente).then(() => {
            refreshClientes();
            message.success("Cadastro apagado com sucesso!")
        })
            .catch((error) => {
                message.error("Cadastro não excluído!")
            });

    }

    function confirmar(cliente) {
        handleCloseAtualizar()
        setAtivoConfirmar(true)
        setCliente(cliente)
    }


    async function refreshClientes() {
        clienteService.retrieveAllClientes()
            .then(
                response => {
                    setClientes(response.data);
                }
            )

    }

    function chamarPagina(cliente) {
        setAtivo(true)
        setCliente(cliente)
    }

    function chamarPaginaAtualizar(cliente) {
        handleClose()
        setAtivoAtualizar(true)
        setCliente(cliente)
    }
    const formatarData = (date) => {

        const data = new Date(date);
        const dataFormatada = data.toLocaleDateString('pt-BR', { timeZone: 'UTC' });
        return dataFormatada;
    }
    return (
        <div className={styles.wrapper}>
            <Modal
                id="modCome" show={ativoConfirmar} size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                dialogClassName={styles.ModalConfirmar}
                centered>
                <Modal.Header closeButton onClick={handleCloseConfirmar} className={styles.titulo}>
                    <Modal.Title>
                        Confirmar Exclusão

                    </Modal.Title>
                </Modal.Header>
                <Modal.Body className="text-left">
                    <h4>
                        Certeza que deseja excluir o cadastro de <b>{cliente.nome}</b> agora?
                    </h4>
                    <br />

                </Modal.Body>
                <div className={styles.confirmar}>
                    <Modal.Footer>
                        <Button className={styles.btnNao} onClick={handleCloseConfirmar}
                        >
                            Não
                        </Button>

                        <Button className={styles.btnSim} onClick={() => excluirCadastro(cliente)}>
                            Sim
                        </Button>
                    </Modal.Footer>
                </div>
            </Modal>

            <Modal
                id="modCome" show={ativo} size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                dialogClassName={styles.Modal}
                centered>
                <Modal.Header closeButton onClick={handleClose} className={styles.titulo}>
                    <Modal.Title>

                        {cliente.nome}

                    </Modal.Title>
                </Modal.Header>
                <Modal.Body className="text-left">

                    <Row>
                        <Col md={6} className={styles.separador}>


                            <h6 className={styles.campo}>E-mail: </h6>
                            <p>{cliente.email}</p>


                        </Col>

                        <Col md={6} className={styles.separador}>


                            <h6 className={styles.campo}>Telefone: </h6>
                            <p>{cliente.telefone}</p>


                        </Col>


                        <div className={styles.atualizar}>
                            <Button class="btn btn-success" md={4} onClick={() => chamarPaginaAtualizar(cliente)}>Atualizar Dados</Button>
                        </div>

                    </Row>

                </Modal.Body>
            </Modal>

            <Modal
                id="modCome" show={ativoAtualizar} size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                dialogClassName={styles.Modal}
                centered>
                <Modal.Header closeButton onClick={handleCloseAtualizar} className={styles.titulo}>
                    <Modal.Title>

                        {cliente.nome} - Atualizar Dados

                    </Modal.Title>
                </Modal.Header>
                <Modal.Body className="text-left">

                    <Form form={form} className={styles.stepsForm} onFinish={onFinish}>
                        <Row>
                            <Col md={6} className={styles.separador}>

                                <h6 className={styles.campo}>Nome: </h6>
                                <p>{cliente.nome}</p>
                                <Form.Item

                                    name="nome"
                                >
                                    <Input placeholder="Atualize o nome" />
                                </Form.Item>


                            </Col>

                            <Col md={6} className={styles.separador}>


                                <h6 className={styles.campo}>E-mail: </h6>
                                <p>{cliente.email}</p>
                                <Form.Item

                                    name="email"
                                >
                                    <Input placeholder="Atualize o email" />
                                </Form.Item>


                            </Col>

                            <Col md={6} className={styles.separador}>

                                <h6 className={styles.campo}>Telefone: </h6>
                                <p>{cliente.telefone}</p>
                                <Form.Item

                                    name="telefone">

                                    <InputTel />
                                </Form.Item>

                            </Col>
                        </Row>

                        <Row>
                            <Col md={11} >
                                <button className={styles.buttonSubmit} type="submit" >
                                    Salvar
                                </button>
                            </Col>
                            <Col md={1}>
                                <Button className={styles.buttonExcluir} onClick={() => confirmar(cliente)} type="submit">
                                    <img src="/excluir.png" alt="excluir" />
                                </Button>
                            </Col>
                        </Row>
                    </Form>
                </Modal.Body>
            </Modal>

            <Header />
            <main className={styles.ListagemContainer}>
                <header className={styles.topo}>
                    <h1 className={styles.titulo}>Imobiliária Vilas Boas</h1>
                </header>
                <PesquisaClientes setClientes={setClientes} />
                <Row>

                    <div className={styles.itens}>
                        {clientes.map(cliente => {
                            return (
                                <Col md={4} onClick={() => chamarPagina(cliente)}>
                                    <ListItem
                                        key={cliente.idCliente}
                                        nome={cliente.nome}

                                    />
                                </Col>
                            )
                        })}
                    </div>

                </Row>
            </main>
        </div>
    );
}