import styles from '../Listagem/styles.module.scss'
import ListItem from '../../Components/ListItemClientes';
import proprietarioService from "../../services/proprietarioService";
import InputMask from "react-input-mask";

import { Form, Input, message, Button } from 'antd';
import { Modal } from "react-bootstrap";
import { Header } from '../../Components/HeaderADM';
import { useEffect, useState } from 'react';
import { Row, Col } from 'react-bootstrap';
import PesquisaProprietarios from '../../Components/PesquisaProprietarios';


const InputTel = (props) => (
    <InputMask mask="(99)99999-9999"
        value={props.value}
        onChange={props.onChange}
        placeholder="Insira o número de telefone" />
);

export default function Clientes() {

    const [ativo, setAtivo] = useState(false);
    const [ativoAtualizar, setAtivoAtualizar] = useState(false);
    const [ativoConfirmar, setAtivoConfirmar] = useState(false);
    const handleClose = () => setAtivo(false);
    const handleCloseAtualizar = () => setAtivoAtualizar(false);
    const handleCloseConfirmar = () => setAtivoConfirmar(false);
    const [proprietarios, setProprietarios] = useState([]);
    const [proprietario, setProprietario] = useState([]);

    useEffect(() => {
        refreshProprietarios();
        return () => {
        }
    }, [])

    const [form] = Form.useForm();

    const onFinish = (values) => {

        handleCloseAtualizar();
        form.resetFields();

        values.idProprietario = proprietario.idProprietario;

        if (values.nome == null) {
            values.nome = proprietario.nome;
        }
        if (values.email == null) {
            values.email = proprietario.email;
        }
        if (values.telefone == null) {
            values.telefone = proprietario.telefone;
        }

        proprietarioService.updateProprietario(values).then(() => {
            refreshProprietarios();
            message.success("Cadastro Atualizado com sucesso!")
        })
            .catch((error) => {
                message.error("Cadastro não atualizado!")
            });
    }

    const excluirCadastro = (values) => {

        handleCloseConfirmar();
        proprietarioService.deleteProprietario(values.idProprietario).then(() => {
            refreshProprietarios();
            message.success("Cadastro apagado com sucesso!")
        })
            .catch((error) => {
                message.error("Cadastro não excluído!")
            });

    }

    function confirmar(proprietario) {
        handleCloseAtualizar()
        setAtivoConfirmar(true)
        setProprietario(proprietario)
    }


    async function refreshProprietarios() {
        proprietarioService.retrieveAllProprietarios()
            .then(
                response => {
                    setProprietarios(response.data);
                }
            )

    }

    function chamarPagina(proprietario) {
        setAtivo(true)
        setProprietario(proprietario)
    }

    function chamarPaginaAtualizar(proprietario) {
        handleClose()
        setAtivoAtualizar(true)
        setProprietario(proprietario)
    }

    return (
        <div className={styles.wrapper}>
            <Modal
                id="modCome" show={ativoConfirmar} size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                dialogClassName={styles.ModalConfirmar}
                centered>
                <Modal.Header closeButton onClick={handleCloseConfirmar} className={styles.titulo}>
                    <Modal.Title>
                        Confirmar Exclusão

                    </Modal.Title>
                </Modal.Header>
                <Modal.Body className="text-left">
                    <h4>
                        Certeza que deseja excluir o cadastro de <b>{proprietario.nome}</b> agora?
                    </h4>
                    <br />

                </Modal.Body>
                <div className={styles.confirmar}>
                    <Modal.Footer>
                        <Button className={styles.btnNao} onClick={handleCloseConfirmar}
                        >
                            Não
                        </Button>

                        <Button className={styles.btnSim} onClick={() => excluirCadastro(proprietario)}>
                            Sim
                        </Button>
                    </Modal.Footer>
                </div>
            </Modal>

            <Modal
                id="modCome" show={ativo} size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                dialogClassName={styles.Modal}
                centered>
                <Modal.Header closeButton onClick={handleClose} className={styles.titulo}>
                    <Modal.Title>

                        {proprietario.nome}

                    </Modal.Title>
                </Modal.Header>
                <Modal.Body className="text-left">

                    <Row>
                        <Col md={6} className={styles.separador}>

                            <h6 className={styles.campo}>E-mail: </h6>
                            <p>{proprietario.email}</p>

                        </Col>

                        <Col md={6} className={styles.separador}>

                            <h6 className={styles.campo}>Telefone: </h6>
                            <p>{proprietario.telefone}</p>

                        </Col>


                        <div className={styles.atualizar}>
                            <Button class="btn btn-success" md={4} onClick={() => chamarPaginaAtualizar(proprietario)}>Atualizar Dados</Button>
                        </div>

                    </Row>

                </Modal.Body>
            </Modal>

            <Modal
                id="modCome" show={ativoAtualizar} size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                dialogClassName={styles.Modal}
                centered>
                <Modal.Header closeButton onClick={handleCloseAtualizar} className={styles.titulo}>
                    <Modal.Title>

                        {proprietario.nome} - Atualizar Dados

                    </Modal.Title>
                </Modal.Header>
                <Modal.Body className="text-left">

                    <Form form={form} className={styles.stepsForm} onFinish={onFinish}>
                        <Row>
                            <Col md={6} className={styles.separador}>

                                <h6 className={styles.campo}>Nome: </h6>
                                <p>{proprietario.nome}</p>
                                <Form.Item

                                    name="nome"
                                >
                                    <Input placeholder="Atualize o nome" />
                                </Form.Item>

                            </Col>

                            <Col md={6} className={styles.separador}>

                                <h6 className={styles.campo}>E-mail: </h6>
                                <p>{proprietario.email}</p>
                                <Form.Item

                                    name="email"
                                >
                                    <Input placeholder="Atualize o email" />
                                </Form.Item>


                            </Col>

                            <Col md={6} className={styles.separador}>

                                <h6 className={styles.campo}>Telefone: </h6>
                                <p>{proprietario.telefone}</p>
                                <Form.Item

                                    name="telefone">

                                    <InputTel />
                                </Form.Item>

                            </Col>
                        </Row>

                        <Row>
                            <Col md={11} >
                                <button className={styles.buttonSubmit} type="submit" >
                                    Salvar
                                </button>
                            </Col>
                            <Col md={1}>
                                <Button className={styles.buttonExcluir} onClick={() => confirmar(proprietario)} type="submit">
                                    <img src="/excluir.png" alt="excluir" />
                                </Button>
                            </Col>
                        </Row>
                    </Form>
                </Modal.Body>
            </Modal>

            <Header />
            <main className={styles.ListagemContainer}>
                <header className={styles.topo}>
                    <h1 className={styles.titulo}>Imobiliária Vilas Boas</h1>
                </header>
                <PesquisaProprietarios setProprietarios={setProprietarios} />
                <Row>

                    <div className={styles.itens}>
                        {proprietarios.map(proprietario => {
                            return (
                                <Col md={4} onClick={() => chamarPagina(proprietario)}>
                                    <ListItem
                                        key={proprietario.idCliente}
                                        nome={proprietario.nome}

                                    />
                                </Col>
                            )
                        })}
                    </div>

                </Row>
            </main>
        </div>
    );
}