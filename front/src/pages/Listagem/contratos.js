import styles from '../Listagem/styles.module.scss'
import ListItem from '../../Components/ListItemContratos';
import locacaoService from "../../services/locacaoService";
import clienteService from '../../services/clienteService';
import imovelService from '../../services/imovelService';
import PesquisaContratos from '../../Components/PesquisaContratos';


import { Form, Input, message, Button } from 'antd';
import { Modal } from "react-bootstrap";
import { Header } from '../../Components/HeaderADM';
import { useEffect, useState } from 'react';
import { Row, Col } from 'react-bootstrap';



export default function Clientes() {

    const [ativo, setAtivo] = useState(false);
    const [ativoAtualizar, setAtivoAtualizar] = useState(false);
    const [ativoConfirmar, setAtivoConfirmar] = useState(false);
    const handleClose = () => setAtivo(false);
    const handleCloseAtualizar = () => setAtivoAtualizar(false);
    const handleCloseConfirmar = () => setAtivoConfirmar(false);
    const [imoveis, setImoveis] = useState([]);
    const [imovel, setImovel] = useState([]);
    const [contrato, setContrato] = useState([]);
    const [contratos, setContratos] = useState([]);
    const [cliente, setCliente] = useState([]);
    const [clientes, setClientes] = useState([]);

    useEffect(() => {
        refreshContratos();
        return () => {
        }
    }, [])

    const [form] = Form.useForm();

    const onFinish = (values) => {

        handleCloseAtualizar();
        form.resetFields();

        values.idLocacao = contrato.idLocacao;

        if (values.contrato == null) {
            values.contrato = contrato.contrato;
        }
        if (values.data_fim == null) {
            values.data_fim = contrato.data_fim;
        }

        if (values.data_inicio == null) {
            values.data_inicio = contrato.data_inicio;
        }

        locacaoService.updateLocacao(values).then(() => {
            refreshContratos();
            message.success("Cadastro Atualizado com sucesso!")
        })
            .catch((error) => {
                message.error("Cadastro não atualizado!")
            });
    }

    const excluirCadastro = (values) => {

        handleCloseConfirmar();
        locacaoService.deleteLocacao(values.idLocacao).then(() => {
            refreshContratos();
            message.success("Cadastro apagado com sucesso!")
        })
            .catch((error) => {
                message.error("Cadastro não excluído!")
            });

    }

    function confirmar(contrato) {
        handleCloseAtualizar()
        setAtivoConfirmar(true)
        setContrato(contrato)
    }


    async function refreshContratos() {
        locacaoService.retrieveAllLocacoes()
            .then(
                response => {
                    setContratos(response.data);
                }
            )

        clienteService.retrieveAllClientes()
            .then(
                response => {
                    setClientes(response.data);
                }
            )

        imovelService.retrieveAllImoveis()
            .then(
                response => {
                    setImoveis(response.data);
                }
            )

    }

    function chamarPagina(contrato) {
        setAtivo(true)
        setContrato(contrato)
    }

    function chamarPaginaAtualizar(contrato) {
        handleClose()
        setAtivoAtualizar(true)
        setContrato(contrato)
    }

    const formatarData = (date) => {
        const data = new Date(date);
        const dataFormatada = data.toLocaleDateString('pt-BR', { timeZone: 'UTC' });
        return dataFormatada;
    }

    return (
        <div className={styles.wrapper}>
            <Modal
                id="modCome" show={ativoConfirmar} size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                dialogClassName={styles.ModalConfirmar}
                centered>
                <Modal.Header closeButton onClick={handleCloseConfirmar} className={styles.titulo}>
                    <Modal.Title>
                        Confirmar Exclusão

                    </Modal.Title>
                </Modal.Header>
                <Modal.Body className="text-left">
                    <h4>
                        Certeza que deseja excluir o contrato <b>{contrato.idLocacao}</b> agora?
                    </h4>
                    <br />

                </Modal.Body>
                <div className={styles.confirmar}>
                    <Modal.Footer>
                        <Button className={styles.btnNao} onClick={handleCloseConfirmar}
                        >
                            Não
                        </Button>

                        <Button className={styles.btnSim} onClick={() => excluirCadastro(contrato)}>
                            Sim
                        </Button>
                    </Modal.Footer>
                </div>
            </Modal>

            <Modal
                id="modCome" show={ativo} size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                dialogClassName={styles.Modal}
                centered>
                <Modal.Header closeButton onClick={handleClose} className={styles.titulo}>
                    <Modal.Title>

                        {contrato.imovel && contrato.imovel.tipo}-{contrato.cliente && contrato.cliente.nome}

                    </Modal.Title>
                </Modal.Header>
                <Modal.Body className="text-left">

                    <Row>

                        <Col md={4} className={styles.separador}>

                            <h6 className={styles.campo}>Contrato: </h6>
                            <p>{contrato.contrato}</p>

                        </Col>

                        <Col md={4} className={styles.separador}>

                            <h6 className={styles.campo}>Data de Início do Contrato: </h6>
                            <p>{formatarData(contrato.data_inicio)}</p>

                        </Col>

                        <Col md={4} className={styles.separador}>

                            <h6 className={styles.campo}>Data de Finalização do Contrato: </h6>
                            <p>{formatarData(contrato.data_fim)}</p>

                        </Col>

                        <Col md={4} className={styles.separador}>

                            <h6 className={styles.campo}>Cliente: </h6>
                            <p>{contrato.cliente && contrato.cliente.nome}</p>

                        </Col>

                        <Col md={4} className={styles.separador}>

                            <h6 className={styles.campo}>Imovel: </h6>
                            <p>{contrato.imovel && contrato.imovel.tipo}, <b>bairro:</b> {contrato.imovel && contrato.imovel.bairro}</p>

                        </Col>

                        <Col md={4} className={styles.separador}>

                            <h6 className={styles.campo}>Proprietário: </h6>
                            <p>{contrato.imovel && contrato.imovel.proprietario && contrato.imovel.proprietario.nome}</p>

                        </Col>



                        <div className={styles.atualizar}>
                            <Button class="btn btn-success" md={4} onClick={() => chamarPaginaAtualizar(contrato)}>Atualizar Dados</Button>
                        </div>

                    </Row>

                </Modal.Body>
            </Modal>

            <Modal
                id="modCome" show={ativoAtualizar} size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                dialogClassName={styles.Modal}
                centered>
                <Modal.Header closeButton onClick={handleCloseAtualizar} className={styles.titulo}>
                    <Modal.Title>

                        {contrato.imovel && contrato.imovel.tipo}-{contrato.cliente && contrato.cliente.nome}

                    </Modal.Title>
                </Modal.Header>
                <Modal.Body className="text-left">

                    <Form form={form} className={styles.stepsForm} onFinish={onFinish}>
                        <Row>
                            <Col md={4} className={styles.field}>
                                <Form.Item
                                    label="Contrato:"
                                    name="contrato"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Altere o status do contrato',
                                        },
                                    ]}>
                                    <select >
                                        <option value="" disabled selected>Selecione o status</option>
                                        <option value="Ativo">Ativo</option>
                                        <option value="Inativo">Inativo</option>
                                    </select>
                                </Form.Item>
                            </Col>

                            <Col md={4} className={styles.field}>
                                <Form.Item
                                    label="Início do contrato:"
                                    name="data_inicio"
                                >
                                    <p>{formatarData(contrato.data_inicio)}</p>
                                    <Input type="date" />
                                </Form.Item>
                            </Col>
                            <Col md={4} className={styles.field}>
                                <Form.Item
                                    label="Finalização do contrato:"
                                    name="data_fim"
                                >
                                    <p>{formatarData(contrato.data_fim)}</p>
                                    <Input type="date" />
                                </Form.Item>
                            </Col>



                        </Row>

                        <Row>
                            <Col md={11} >
                                <button className={styles.buttonSubmit} type="submit" >
                                    Salvar
                                </button>
                            </Col>
                            <Col md={1}>
                                <Button className={styles.buttonExcluir} onClick={() => confirmar(contrato)} type="submit">
                                    <img src="/excluir.png" alt="excluir" />
                                </Button>
                            </Col>
                        </Row>
                    </Form>
                </Modal.Body>
            </Modal>

            <Header />
            <main className={styles.ListagemContainer}>
                <header className={styles.topo}>
                    <h1 className={styles.titulo}>Imobiliária Vilas Boas</h1>
                </header>
                <PesquisaContratos setContratos={setContratos} />
                <Row>

                    <div className={styles.itens}>
                        {contratos.map(contrato => {
                            return (
                                <Col md={6} onClick={() => chamarPagina(contrato)}>

                                    <ListItem
                                        key={contrato.idLocacao}
                                        imovel={contrato.imovel && contrato.imovel.tipo}
                                        cliente={contrato.cliente && contrato.cliente.nome}

                                    />
                                </Col>
                            )
                        })}
                    </div>

                </Row>
            </main>
        </div>
    );
}