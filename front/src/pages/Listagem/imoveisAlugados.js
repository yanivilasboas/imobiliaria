import styles from '../Listagem/styles.module.scss'
import ListItem from '../../Components/ListItemImoveis';
import imovelService from "../../services/imovelService";
import locacaoService from '../../services/locacaoService';
import PesquisaImoveisAlugados from '../../Components/PesquisaImoveisAlugados';


import { Form, Input, message, Button } from 'antd';
import { Modal } from "react-bootstrap";
import { Header } from '../../Components/HeaderADM';
import { useEffect, useState } from 'react';
import { Row, Col } from 'react-bootstrap';



export default function Clientes() {

    const [ativo, setAtivo] = useState(false);
    const [ativoAtualizar, setAtivoAtualizar] = useState(false);
    const [ativoConfirmar, setAtivoConfirmar] = useState(false);
    const handleClose = () => setAtivo(false);
    const handleCloseAtualizar = () => setAtivoAtualizar(false);
    const handleCloseConfirmar = () => setAtivoConfirmar(false);
    const [imoveis, setImoveis] = useState([]);
    const [imovel, setImovel] = useState([]);
    const [locacao, setLocacao] = useState([]);
    const [contratos, setContratos] = useState([]);

    useEffect(() => {
        refreshImoveis();
        return () => {
        }
    }, [])

    const [form] = Form.useForm();

    const onFinish = (values) => {

        handleCloseAtualizar();
        form.resetFields();

        values.idImovel = imovel.idImovel;

        if (values.logradouro == null) {
            values.logradouro = imovel.logradouro;
        }
        if (values.numero == null) {
            values.numero = imovel.numero;
        }
        if (values.cidade == null) {
            values.cidade = imovel.cidade;
        }
        if (values.bairro == null) {
            values.bairro = imovel.bairro;
        }
        if (values.estado == null) {
            values.estado = imovel.estado;
        }
        if (values.uf == null) {
            values.uf = imovel.uf;
        }
        if (values.cep == null) {
            values.cep = imovel.cep;
        }

        if (values.tipo == null) {
            values.tipo = imovel.tipo;
        }

        if (values.valor == null) {
            values.valor = imovel.valor;
        }

        imovelService.updateImovel(values).then(() => {
            refreshImoveis();
            message.success("Cadastro Atualizado com sucesso!")
        })
            .catch((error) => {
                message.error("Cadastro não atualizado!")
            });
    }

    const excluirCadastro = (values) => {

        handleCloseConfirmar();
        imovelService.deleteImovel(values.idImovel).then(() => {
            refreshImoveis();
            message.success("Cadastro apagado com sucesso!")
        })
            .catch((error) => {
                message.error("Cadastro não excluído!")
            });

    }

    function confirmar(imovel) {
        handleCloseAtualizar()
        setAtivoConfirmar(true)
        setImovel(imovel)
    }


    async function refreshImoveis() {
        imovelService.retrieveAllImoveis()
            .then(
                response => {
                    setImoveis(response.data);
                }
            )

        locacaoService.retrieveAllLocacoes()
            .then(
                response => {
                    setContratos(response.data);
                }
            )

    }

    function chamarPagina(imovel) {
        setAtivo(true)
        setImovel(imovel)
    }

    function chamarPaginaAtualizar(imovel) {
        handleClose()
        setAtivoAtualizar(true)
        setImovel(imovel)
    }

    return (
        <div className={styles.wrapper}>
            <Modal
                id="modCome" show={ativoConfirmar} size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                dialogClassName={styles.ModalConfirmar}
                centered>
                <Modal.Header closeButton onClick={handleCloseConfirmar} className={styles.titulo}>
                    <Modal.Title>
                        Confirmar Exclusão

                    </Modal.Title>
                </Modal.Header>
                <Modal.Body className="text-left">
                    <h4>
                        Certeza que deseja excluir o imóvel agora?
                    </h4>
                    <br />

                </Modal.Body>
                <div className={styles.confirmar}>
                    <Modal.Footer>
                        <Button className={styles.btnNao} onClick={handleCloseConfirmar}
                        >
                            Não
                        </Button>

                        <Button className={styles.btnSim} onClick={() => excluirCadastro(imovel)}>
                            Sim
                        </Button>
                    </Modal.Footer>
                </div>
            </Modal>

            <Modal
                id="modCome" show={ativo} size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                dialogClassName={styles.Modal}
                centered>
                <Modal.Header closeButton onClick={handleClose} className={styles.titulo}>
                    <Modal.Title>

                        {imovel.tipo}-{imovel.idImovel}

                    </Modal.Title>
                </Modal.Header>
                <Modal.Body className="text-left">

                    <Row>

                        <Col md={4} className={styles.separador}>

                            <h6 className={styles.campo}>Tipo: </h6>
                            <p>{imovel.tipo}</p>

                        </Col>

                        <Col md={4} className={styles.separador}>

                            <h6 className={styles.campo}>Valor: </h6>
                            <p>{imovel.valor}</p>

                        </Col>

                        <Col md={4} className={styles.separador}>

                            <h6 className={styles.campo}>Proprietario: </h6>
                            <p>{imovel.proprietario && imovel.proprietario.nome}</p>

                        </Col>

                        <Row className={styles.separadorTitulo}>
                            <Col md={12}>
                                <h5>Endereço</h5>
                            </Col>
                        </Row>

                        <Col md={2} className={styles.separador}>
                            <h6 className={styles.campo}>Logradouro: </h6>
                            <p>{imovel.logradouro}</p>
                        </Col>
                        <Col md={1} className={styles.separador}>
                            <h6 className={styles.campo}>Nº: </h6>
                            <p>{imovel.numero}</p>
                        </Col>
                        <Col md={2} className={styles.separador}>
                            <h6 className={styles.campo}>Bairro: </h6>
                            <p><p>{imovel.bairro}</p></p>
                        </Col>
                        <Col md={2} className={styles.separador}>
                            <h6 className={styles.campo}>Cidade: </h6>
                            <p><p>{imovel.cidade}</p></p>
                        </Col>
                        <Col md={2} className={styles.separador}>
                            <h6 className={styles.campo}>CEP: </h6>
                            <p><p>{imovel.cep}</p></p>
                        </Col>
                        <Col md={2} className={styles.separador}>
                            <h6 className={styles.campo}>Estado: </h6>
                            <p><p>{imovel.estado}</p></p>
                        </Col>
                        <Col md={1} className={styles.separador}>
                            <h6 className={styles.campo}>UF: </h6>
                            <p><p>{imovel.uf}</p></p>
                        </Col>



                        <div className={styles.atualizar}>
                            <Button class="btn btn-success" md={4} onClick={() => chamarPaginaAtualizar(imovel)}>Atualizar Dados</Button>
                        </div>

                    </Row>

                </Modal.Body>
            </Modal>

            <Modal
                id="modCome" show={ativoAtualizar} size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                dialogClassName={styles.Modal}
                centered>
                <Modal.Header closeButton onClick={handleCloseAtualizar} className={styles.titulo}>
                    <Modal.Title>

                        {imovel.tipo}-{imovel.idImovel}

                    </Modal.Title>
                </Modal.Header>
                <Modal.Body className="text-left">

                    <Form form={form} className={styles.stepsForm} onFinish={onFinish}>
                        <Row>
                            <Col md={6} className={styles.separador}>
                                <Row>

                                    <h6 className={styles.campo}>Valor: </h6>
                                    <p>{imovel.valor}</p>
                                    <Form.Item

                                        name="valor"
                                    >
                                        <Input placeholder="Atualize o valor" />
                                    </Form.Item>

                                </Row>

                            </Col>


                        </Row>

                        <Row>
                            <Col md={11} >
                                <button className={styles.buttonSubmit} type="submit" >
                                    Salvar
                                </button>
                            </Col>
                            <Col md={1}>
                                <Button className={styles.buttonExcluir} onClick={() => confirmar(imovel)} type="submit">
                                    <img src="/excluir.png" alt="excluir" />
                                </Button>
                            </Col>
                        </Row>
                    </Form>
                </Modal.Body>
            </Modal>

            <Header />
            <main className={styles.ListagemContainer}>
                <header className={styles.topo}>
                    <h1 className={styles.titulo}>Imobiliária Vilas Boas</h1>
                </header>
                <PesquisaImoveisAlugados setContratos={setContratos} />
                <Row>

                    <div className={styles.itens}>
                        {contratos.map(locacao => {


                            return (
                                <Col md={3} onClick={() => chamarPagina(imovel)}>

                                    <ListItem
                                        key={locacao.imovel && locacao.imovel.idImovel}
                                        tipo={locacao.imovel && locacao.imovel.tipo}
                                        valor={locacao.imovel && locacao.imovel.valor}
                                        bairro={locacao.imovel && locacao.imovel.bairro}

                                    />
                                </Col>
                            )
                        })}

                    </div>

                </Row>


            </main>
        </div>
    );
}