import axios from 'axios';

const API_URL = "http://localhost:8080"


class LocacaoService{

    retrieveAllLocacoes(){
        return axios.get(`${API_URL}/locacoes`)
    }

    saveLocacoes(locacao){
        return axios.post(`${API_URL}/locacoes`, locacao)
    }
    
    deleteLocacao(idLocacao){
        return axios.delete(`${API_URL}/locacoes/${idLocacao}`)
    }

    updateLocacao(locacao){
        return axios.put(`${API_URL}/locacoes`, locacao)
    }
}

export default new LocacaoService();