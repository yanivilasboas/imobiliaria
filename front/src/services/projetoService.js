import axios from 'axios';


class ProjetoService{

    retrieveAllProjetos(){
        return axios.get(`/publico/projetos`)
    }

    saveProjetos(projeto){
        return axios.post(`/publico/projetos`, projeto)
    }
    
    deleteProjeto(idProjeto){
        return axios.delete(`/publico/projetos/${idProjeto}`)
    }

    updateProjetos(projeto){
        return axios.put(`/publico/projetos`, projeto)
    }
}

export default new ProjetoService();