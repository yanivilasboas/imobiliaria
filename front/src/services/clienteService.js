import axios from 'axios';

const API_URL = "http://localhost:8080"


class ClienteService{

    retrieveAllClientes(){
        return axios.get(`${API_URL}/clientes`)
    }

    saveClientes(cliente){
        return axios.post(`${API_URL}/clientes`, cliente)
    }
    
    deleteCliente(idCliente){
        return axios.delete(`${API_URL}/clientes/${idCliente}`)
    }

    updateProjetos(cliente){
        return axios.put(`${API_URL}/clientes`, cliente)
    }
}

export default new ClienteService();