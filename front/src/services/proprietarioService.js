import axios from 'axios';

const API_URL = "http://localhost:8080"


class ProprietarioService{

    retrieveAllProprietarios(){
        return axios.get(`${API_URL}/proprietarios`);
    }

    saveProprietarios(proprietario){
        return axios.post(`${API_URL}/proprietarios`, proprietario)
    }
    
    deleteProprietario(idProprietario){
        return axios.delete(`${API_URL}/proprietarios/${idProprietario}`)
    }

    updateProprietario(proprietario){
        return axios.put(`${API_URL}/proprietarios`, proprietario)
    }
}

export default new ProprietarioService();