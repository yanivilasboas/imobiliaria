import axios from 'axios';

const API_URL = "http://localhost:8080"


class ImovelService{

    retrieveAllImoveis(){
        return axios.get(`${API_URL}/imoveis`)
    }

    saveImoveis(imovel){
        return axios.post(`${API_URL}/imoveis`, imovel)
    }
    
    deleteImovel(idImovel){
        return axios.delete(`${API_URL}/imoveis/${idImovel}`)
    }

    updateImovel(imovel){
        return axios.put(`${API_URL}/imoveis`, imovel)
    }
}

export default new ImovelService();