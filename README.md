# Imobiliária Vilas Boas

Desafio Técnico - Especialista em Sistemas I - DEV (CIDC)

Sistema desenvolvido com Spring Boot e React, utilizando os softwares VSCode e Intellij IDEA.

*Para funcionamento do projeto, basta criar um banco de dados com o nome imobiliaria e executar o código Java. Lembrando de alterar a senha no arquivo "application.properties". Ao abrir o front-end, executar o comando yarn para baixar dependências e em seguida, executar o projeto com yarn start.


