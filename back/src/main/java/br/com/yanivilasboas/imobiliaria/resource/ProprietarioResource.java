package br.com.yanivilasboas.imobiliaria.resource;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import br.com.yanivilasboas.imobiliaria.domain.Proprietario;
import br.com.yanivilasboas.imobiliaria.repository.ProprietarioRepository;

@RestController
@RequestMapping("/proprietarios")
@CrossOrigin(origins = "http://localhost:3000")
public class ProprietarioResource {

    @Autowired
    private ProprietarioRepository proprietarioRepository;

    @GetMapping
    public List<Proprietario> listarTodos(){
        return proprietarioRepository.findAll();
    }
    @GetMapping("/{idProprietario}")
    public Proprietario buscarpeloId(@PathVariable Long idProprietario) {
        return proprietarioRepository.findById(idProprietario).orElse(null);
    }

    @DeleteMapping("/{idProprietario}")
    public void remover(@PathVariable Long idProprietario) {
        proprietarioRepository.deleteById(idProprietario);
    }

    @PostMapping
    public Proprietario cadastrar(@RequestBody Proprietario proprietario ){
        return proprietarioRepository.save(proprietario);
    }

    @PutMapping
    public Proprietario atualizar(@RequestBody Proprietario proprietario) {
        return proprietarioRepository.save(proprietario);
    }
}
