package br.com.yanivilasboas.imobiliaria.domain;

public enum TipoImovel {
    Apartamento,
    Kitnet,
    Casa
}
