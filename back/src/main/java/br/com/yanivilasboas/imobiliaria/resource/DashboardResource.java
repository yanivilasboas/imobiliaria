package br.com.yanivilasboas.imobiliaria.resource;

import br.com.yanivilasboas.imobiliaria.domain.Dashboard;
import br.com.yanivilasboas.imobiliaria.repository.*;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dashboard")
@CrossOrigin(origins = "http://localhost:3000")
@RequiredArgsConstructor
public class DashboardResource {

    private final ClienteRepository clienteRepository;
    private final LocacaoRepository locacaoRepository;
    private final ImovelRepository imovelRepository;
    private final ProprietarioRepository proprietarioRepository;

    @PostMapping
    public ResponseEntity<Dashboard> filtro() {

        Dashboard dashboard = new Dashboard();

        dashboard.setQtdClientes(clienteRepository.count());
        dashboard.setQtdLocacoes(locacaoRepository.count());
        dashboard.setQtdImoveis(imovelRepository.count());
        dashboard.setQtdProprietarios(proprietarioRepository.count());

        return ResponseEntity.ok(dashboard);
    }
}
