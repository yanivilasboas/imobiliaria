package br.com.yanivilasboas.imobiliaria.repository;

import br.com.yanivilasboas.imobiliaria.domain.Imovel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImovelRepository extends JpaRepository<Imovel, Long> {
}
