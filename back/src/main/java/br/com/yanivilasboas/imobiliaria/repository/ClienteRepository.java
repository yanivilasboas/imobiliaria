package br.com.yanivilasboas.imobiliaria.repository;

import br.com.yanivilasboas.imobiliaria.domain.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {
}
