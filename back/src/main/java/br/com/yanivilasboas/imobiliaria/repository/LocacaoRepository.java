package br.com.yanivilasboas.imobiliaria.repository;

import br.com.yanivilasboas.imobiliaria.domain.Locacao;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocacaoRepository extends JpaRepository<Locacao, Long> {
}
