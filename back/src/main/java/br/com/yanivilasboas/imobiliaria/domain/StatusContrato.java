package br.com.yanivilasboas.imobiliaria.domain;

public enum StatusContrato {
    Ativo,
    Inativo
}
