package br.com.yanivilasboas.imobiliaria;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImobiliariaVilasboasApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImobiliariaVilasboasApplication.class, args);
	}

}
