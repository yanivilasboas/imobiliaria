package br.com.yanivilasboas.imobiliaria.repository;

import br.com.yanivilasboas.imobiliaria.domain.Proprietario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProprietarioRepository extends JpaRepository<Proprietario, Long> {
}
