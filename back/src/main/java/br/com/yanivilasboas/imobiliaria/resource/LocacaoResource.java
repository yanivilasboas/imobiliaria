package br.com.yanivilasboas.imobiliaria.resource;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import br.com.yanivilasboas.imobiliaria.domain.Locacao;
import br.com.yanivilasboas.imobiliaria.repository.LocacaoRepository;

@RestController
@RequestMapping("/locacoes")
@CrossOrigin(origins = "http://localhost:3000")
public class LocacaoResource {

    @Autowired
    private LocacaoRepository locacaoRepository;

    @GetMapping
    public List<Locacao> listarTodos(){
        return locacaoRepository.findAll();
    }

    @GetMapping("/{idLocacao}")
    public Locacao buscarpeloId(@PathVariable Long idLocacao) {
        return locacaoRepository.findById(idLocacao).orElse(null);
    }

    @DeleteMapping("/{idLocacao}")
    public void remover(@PathVariable Long idLocacao) {
        locacaoRepository.deleteById(idLocacao);
    }

    @PostMapping
    public Locacao cadastrar(@RequestBody Locacao locacao){
        return locacaoRepository.save(locacao);
    }

    @PutMapping
    public Locacao atualizar(@RequestBody Locacao locacao) {
        return locacaoRepository.save(locacao);
    }
}
