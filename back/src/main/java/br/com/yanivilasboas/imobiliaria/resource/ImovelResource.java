package br.com.yanivilasboas.imobiliaria.resource;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import br.com.yanivilasboas.imobiliaria.domain.Imovel;
import br.com.yanivilasboas.imobiliaria.repository.ImovelRepository;

@RestController
@RequestMapping("/imoveis")
@CrossOrigin(origins = "http://localhost:3000")
public class ImovelResource {

    @Autowired
    private ImovelRepository imovelRepository;

    @GetMapping
    public List<Imovel> listarTodos(){
        return imovelRepository.findAll();
    }

    @GetMapping("/{idImovel}")
    public Imovel buscarpeloId(@PathVariable Long idImovel) {
        return imovelRepository.findById(idImovel).orElse(null);
    }

    @DeleteMapping("/{idImovel}")
    public void remover(@PathVariable Long idImovel) {
        imovelRepository.deleteById(idImovel);
    }

    @PostMapping
    public Imovel cadastrar(@RequestBody Imovel imovel ){
        return imovelRepository.save(imovel);
    }

    @PutMapping
    public Imovel atualizar(@RequestBody Imovel imovel) {
        return imovelRepository.save(imovel);
    }

}
