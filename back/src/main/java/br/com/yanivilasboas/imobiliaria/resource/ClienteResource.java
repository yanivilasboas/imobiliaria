package br.com.yanivilasboas.imobiliaria.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import br.com.yanivilasboas.imobiliaria.domain.Cliente;
import br.com.yanivilasboas.imobiliaria.repository.ClienteRepository;

@RestController
@RequestMapping("/clientes")
@CrossOrigin(origins = "http://localhost:3000")
public class ClienteResource {

    @Autowired
    private ClienteRepository clienteRepository;

    @GetMapping
    public List<Cliente> listarTodos(){
        return clienteRepository.findAll();
    }

    @GetMapping("/{iCliente}")
    public Cliente buscarpeloId(@PathVariable Long idCliente) {
        return clienteRepository.findById(idCliente).orElse(null);
    }

    @DeleteMapping("/{idCliente}")
    public void remover(@PathVariable Long idCliente) {
        clienteRepository.deleteById(idCliente);
    }

    @PostMapping
    public Cliente cadastrar(@RequestBody Cliente cliente ){
        return clienteRepository.save(cliente);
    }

    @PutMapping
    public Cliente atualizar(@RequestBody Cliente cliente) {
        return clienteRepository.save(cliente);
    }

}
