package br.com.yanivilasboas.imobiliaria.domain;


import lombok.Data;

@Data
public class Dashboard {

    private Long qtdClientes;
    private Long qtdLocacoes;
    private Long qtdImoveis;
    private Long qtdProprietarios;

}
